// help							Display help on all the commands
// tab Keys						Display the list of all commands.
// Up/Down Keys					See previous commands applied in REPL.
// .save filename				Save current Node REPL session to a file.
// .load filename				Load the specified file in the current Node REPL session.
// ctrl + c						Terminate the current command.
// ctrl + c (twice)				Exit from the REPL.
// ctrl + d						Exit from the REPL.
// .break						Exit from multiline expression.
// .clear						Exit from multiline expression.


// What is Buffer
// Node.js includes an additional data type called Buffer (not available in browser's JavaScript). Buffer is mainly used to store binary data, while reading from a file or receiving packets over the network.


// Each module in Node.js has its own context, so it cannot interfere with other modules or pollute global scope. Also, each module can be placed in a separate .js file under a separate folder.



// Core Module					Description

// http							http module includes classes, methods and events to create Node.js http server.
// url							url module includes methods for URL resolution and parsing.
// querystring					querystring module includes methods to deal with query string.
// path							path module includes methods to deal with file paths.
// fs							fs module includes classes, methods, and events to work with file I/O.
// util							util module includes utility functions useful for programmers.




// flags use in node.js
// basically three type of operation we perform in it """read""", """write""" and """append"""
// Flag						Description
// r						Open file for reading. An exception occurs if the file does not exist.
// r+						Open file for reading and writing. An exception occurs if the file does not exist.
// rs						Open file for reading in synchronous mode.
// rs+						Open file for reading and writing, telling the OS to open it synchronously. See notes for 'rs' about using this with caution.
// w						Open file for writing. The file is created (if it does not exist) or truncated (if it exists).
// wx						Like 'w' but fails if path exists.
// w+						Open file for reading and writing. The file is created (if it does not exist) or truncated (if it exists).
// wx+						Like 'w+' but fails if path exists.
// a						Open file for appending. The file is created if it does not exist.
// ax						Like 'a' but fails if path exists.
// a+						Open file for reading and appending. The file is created if it does not exist.
// ax+						Like 'a+' but fails if path exists.




// A buffer is an area of memory. ... It represents a fixed-size chunk of memory (can't be resized) allocated outside of the V8 JavaScript engine. You can think of a buffer like an array of integers, which each represent a byte of data. It is implemented by the Node Buffer class.




// Important method of fs module

// Method													Description

// fs.readFile(fileName [,options], callback)				Reads existing file.
// fs.writeFile(filename, data[, options], callback)		Writes to the file. If file exists then overwrite the content otherwise creates new file.
// fs.open(path, flags[, mode], callback)					Opens file for reading or writing.
// fs.rename(oldPath, newPath, callback)					Renames an existing file.
// fs.chown(path, uid, gid, callback)						Asynchronous chown.
// fs.stat(path, callback)									Returns fs.stat object which includes important file statistics.
// fs.link(srcpath, dstpath, callback)						Links file asynchronously.
// fs.symlink(destination, path[, type], callback)			Symlink asynchronously.
// fs.rmdir(path, callback)									Renames an existing directory.
// fs.mkdir(path[, mode], callback)							Creates a new directory.
// fs.readdir(path, callback)								Reads the content of the specified directory.
// fs.utimes(path, atime, mtime, callback)					Changes the timestamp of the file.
// fs.exists(path, callback)								Determines whether the specified file exists or not.
// fs.access(path[, mode], callback)						Tests a user's permissions for the specified file.
// fs.appendFile(file, data[, options], callback)			Appends new content to the existing file.



// The following table lists important debugging commands:

// Command						Description

// next						Stop at the next statement.
// cont						Continue execute and stop at the debugger statement if any.
// step						Step in function.
// 							out	Step out of function.
// watch					Add the expression or variable into watch.
// watcher					See the value of all expressions and variables added into watch.
// Pause					Pause running code.


// These objects expose an eventEmitter.on() function that allows one or more functions to be attached to named events emitted by the object. When the EventEmitter object emits an event, all of the functions attached to that specific event are called synchronously.





// The following table lists all the important methods of EventEmitter class.

// EventEmitter Methods								Description

// emitter.addListener(event, listener)				Adds a listener to the end of the listeners array for the specified event. No checks are made to see if the listener has already been added.
// emitter.on(event, listener)						Adds a listener to the end of the listeners array for the specified event. No checks are made to see if the listener has already been added. It can also be called as an alias of emitter.addListener()
// emitter.once(event, listener)					Adds a one time listener for the event. This listener is invoked only the next time the event is fired, after which it is removed.
// emitter.removeListener(event, listener)			Removes a listener from the listener array for the specified event. Caution: changes array indices in the listener array behind the listener.
// emitter.removeAllListeners([event])				Removes all listeners, or those of the specified event.
// emitter.setMaxListeners(n)						By default EventEmitters will print a warning if more than 10 listeners are added for a particular event.
// emitter.getMaxListeners()						Returns the current maximum listener value for the emitter which is either set by emitter.setMaxListeners(n) or defaults to EventEmitter.defaultMaxListeners.
// emitter.listeners(event)							Returns a copy of the array of listeners for the specified event.
// emitter.emit(event[, arg1][, arg2][, ...])		Raise the specified events with the supplied arguments.
// emitter.listenerCount(type)						Returns the number of listeners listening to the type of event.



























