// how to use event listner and eventhandeler

var emitter = require('events').EventEmitter;//this line of code is must to write

var em = new emitter();

//Subscribe FirstEvent
em.addListener('FirstEvent', function (data) {
    console.log('First subscriber: ' + data);
});

//Subscribe SecondEvent
em.on('SecondEvent', function (data) {
    console.log('First subscriber: ' + data);
});
// Raising FirstEvent
// here first event called by name and provided the data to use and display over there
// and we can alse use multiple parameters in it
em.emit('FirstEvent', 'This is my first Node.js event emitter example.');

// Raising SecondEvent
// same as first event listner called
em.emit('SecondEvent', 'This is my second Node.js event emitter example.');
