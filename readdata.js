// way to read the file using node

var http = require('http'); 

var server = http.createServer(function (req, res) {   
   
	// and this is the way to use text file to use to display data over web page
 //   	var fs1 = require('fs');
	// var data = fs1.readFileSync('TestFile.txt', 'utf8');
	// console.log(data);
    if (req.url == '/') { //check the URL of the current request
            res.writeHead(200, { 'Content-Type': 'text/html' });
            // res.write(data); 
            res.write("ge");  
            res.end();  
    }
var fs = require('fs');

// this is the way to print text in console but twice
fs.readFile('TestFile.txt', 'utf8', function (err, data) {
    if (err) throw err;
		console.log("data");
		// /favicon.ico problem 
		// this code will return twice the result over console because of first request based on favicon.io and after that second request is based on actual request
		// /favicon.ico is nothig but the logo used for any web site which is displayed in the left cornet of tab  
		console.log(req.url);
		console.log(req.headers);
	});

	});

server.listen(5000);

console.log('Node.js web server at port 5000 is running..')

