var http = require('http'); 

var server = http.createServer(function (req, res) {   

var fs = require('fs');

fs.writeFile('test.txt', 'Hello World!','utf-8', function (err) { 
    if (err)
        console.log(err);
    else
        console.log('Write operation complete.');
});

var fs = require('fs');

fs.appendFile('test.txt', 'Hello World!', function (err) { 
	// always remember if only one line will come after """if""" condition than we don't need any parantsis or brackets for defining there range or scope  
    if (err)
        console.log(err);
    else
        console.log('Append operation complete.');
});

var fs1 = require('fs');
var data = fs1.readFileSync('test.txt', 'utf8');
if (req.url == '/') { //check the URL of the current request
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data); 
        res.end();  
    }
});
server.listen(5000);

console.log('Node.js web server at port 5000 is running..')

