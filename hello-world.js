// in node js we use """const""" in place of """var""" in javaScript
//to create the server we must declear the host and port for redirection


// in """required""" is used for inporting modules in node
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;


// this is server for node js must to create
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello, World!\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});