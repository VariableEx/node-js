// get the reference of EventEmitter class of events module
// and in that require('events') """events""" is a modeule
var events = require('events');

//create an object of EventEmitter class by using above reference
// and here """EventEmitter()""" is a pre-defined class used for event handeling 
var em = new events.EventEmitter();

//Subscribe for FirstEvent
// here FirstEvent is a event name to display nothing else 
var z = em.on('FirstEvent', function (data) {
    console.log('First subscriber: ' + data);
    // and rest of the code goes here
});
console.log(z)
// Raising FirstEvent
// call the finction for display the value of the event and this call goes to only those function whose name is same like FirstEvent
em.emit('FirstEvent', 'This is my first Node.js event emitter example.');